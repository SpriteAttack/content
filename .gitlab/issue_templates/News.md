<!-- This is for *short* news items to be added to the main website -->

## Title
Proposed title for the news item here.

## Body
Draft the text for the news item here.

## Tasks
* [ ] Preliminary draft
* [ ] Review and copy edit
* [ ] Final draft
* [ ] Add to website, unpublished
* [ ] Publish the news item to the main website

## Additional Publication Channels
<!-- remove spaces between @ and name when it's time to distribute the tasks -->

* [ ] Twitter post ( @ ryangorley )
* [ ] Facebook group post ( @ t1mj0nes )
* [ ] Mastodon post ( @ ryangorley )
* [ ] Forums ( @ brynn or @ Lazur )
* [ ] google+ ( @ prokoudine, @ ryangorley or @ CRogers )
* [ ] facebook (@ rsterbal )
