Come for the GSOC code experience, stay for the OS collaborative team environment

Once more, Inkscape has been approved to accept students to work on the project through Google Summer of Code (GSOC) 2020. We are excited about offering students a chance to work with mentors on this open source (OS) project.

When the Inkscape community seeks Google Summer of Code (GSoC) applicants, we're hoping to find people who are curious about the project and the software. Part of the application process requires applicants to complete sample work on the project to demonstrate their skills and understanding.

In the winter of 2019, Valentin (@vanntile) joined the community, tackled several tasks and shared the outline of a project he wanted to work on as a GSoC student.
"The GSoC experience was something I had been waiting for a whole year, since the first time I had heard of a program like this. When I found Inkscape to be one of the organizations, a program I had been using for such a long time, I decided to be one of the mentees," said Valentin, who hails from Bucharest, Romania.

By the summer, our GSoC student not only knew the program, but got involved in our online community. Valentin brought his curiosity, determination and coding skills to the project. Inkscape core developer Tavmjong Bah (@Tav) mentored Valentin during his work.

Valentin proposed working on JavaScript polyfills to improve image rendering in browsers for mesh gradients and SVG hatch fills. While it began there, his contribution evolved to working on mesh gradients and hatches to enable non-technical users of Inkscape to more easily use them.
Read more about Valentin's GSOC project with Inkscape here (LINK).

What could you possibly accomplish during a summer internship with Inkscape, you ask? If you have an idea or a question, get in touch! Let us know what you'd like to work on together. 

Remember that to apply, you will need to have contributed to the Inkscape project.
 
How to Apply
Here are the steps you need to take before applying:
Read the Wiki page (https://wiki.inkscape.org/wiki/index.php/Google_Summer_of_Code).
Read the GSOC student guide.
Introduce yourself on our developers' mailing list, #inkscape-devel on IRC or in our rocket.chat development channel.
Contribute two patches (small code contributions) to the project.
As you are working on your patches, think about the code project you wish to propose to improve Inkscape.
Once you have introduced yourself, discuss your plans with the development team as you will be collaborating on the GSOC application. The wiki link above outlines the Inkscape project, potential GSOC project areas and mentors.
Inkscapers will be happy to answer your questions about how to get involved.
The Student Application Period runs from March 16-31, 2020. For more details, visit the GSOC home page (https://summerofcode.withgoogle.com/). Then head to the Inkscape page on GSOC (https://summerofcode.withgoogle.com/organizations/6070010742571008/) for the specific questions and details
The deadline for applying to the Google Summer of Code is March 31. 
We look forward to meeting you soon.