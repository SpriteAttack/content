INKSCAPE RELEASE ARTICLE FOR 1.0.2

https://wiki.inkscape.org/wiki/index.php/Release_notes/1.0.2



#### **Bug fixes in Inkscape 1.0.2 range from repairing Eraser Tool to improving macOS packaging and more**



Stability and bug fixes are the main focus of the Inkscape 1.0.2 release, including a packaging update for macOS that improves performance over the last release. This release provides fixes for many crashes in general, so updating will provide a smoother experience in Inkscape overall. 

Built with the power of a team of mostly volunteers, this open source vector editor represents the work of many hearts and hands from around the world, ensuring that Inkscape remains available free for everyone to download and enjoy.



**macOS packaging**

Inkscape's packaging has been updated for macOS, which enables better performance, including icons appearing correctly and PDF export producing printable PDF files. The macOS version of Inkscape 1.0.2 is faster than 1.0.1, however, project developers are continuing to improve the macOS version for future releases.



**Bug Fixes**

Inkscape's *Eraser Tool* works once again, which will please every user! Gone are the red lines. Welcome, once again to being able to draw and erase freely!

Crafters and makers seeking to frame and shape text will no longer need to rely on workarounds anymore to use text in Boolean operations or when offsetting text. This means that if users have been holding out with 0.92.5, they'll find the same functionality restored in 1.0.2. Older tutorials will once again be helpful for users seeking to learn how to wrangle text in Inkscape.



**Features**

Two new features made the cut. It's now possible to *deactivate the zoom function* on the middle mouse button and prevent accidental *Canvas rotation* either temporarily for a document or for all new Inkscape windows.



**For more details on specific updates in Inkscape 1.0.2, check out the [Release Notes on the download page](https://inkscape.org/release).**



## Download Inkscape 1.0.2 now

**Head [here to download Inkscape 1.0.2 for Linux, Windows and macOS](https://inkscape.org/release/)**

Version 1.0.2 of this free and open-source vector editor is an update for the stable version 1.0, that was released in May 2020.



## Reach out & join the community

Questions, comments and feedback on Inkscape are welcome. For user support, head to [Inkscape's chat](https://chat.inkscape.org/channel/inkscape_user), where community volunteers field help requests of all sorts. To report bugs, [fill out a report directly at the Inbox](https://gitlab.com/inkscape/inbox/-/issues/).

From programmers and translators to writers, researchers and artists, the Inkscape community is a friendly space to learn and share, build skills and meet others interested in Free Software and vector editing. Visit [Inkscape's Community Page](https://inkscape.org/community/) for ways to connect.

***Draw Freely!***



**Release of 1.1alpha for testing** 

For those seeking more adventure, Inkscape's 1.1alpha release is also available. This is an important step in the launch process for the release of version 1.1, scheduled for May 2021. An important part of this release is to gain valuable feedback from users that will improve the major release in the Spring. 



**New features**

Early adopters will find a *Welcome Screen* upon launching the program. This will help users to quickly set up preferences located on three tabs.  

In the first tab, users can choose the theme, including sticking with the classic Inkscape look and feel. The second tab provides links to contributing and / or donating opportunities, while the third tab makes quick work of choosing the right document size for the project at hand, from images for print, video or social media channels and more.

The *Commands palette* will open by using the "?" key, allowing users to search and click on many functions without worrying about keyboard shortcuts or menus. 

Additionally, when it comes to finding and setting preferences, there's a new search function that will speed up the process. 

Artists will appreciate the new outline overlay mode, where path outlines show through the drawing. This is different than the original outline mode, which only shows the path outlines. 

For more information on what's included in this alpha version, [head here for the release notes](https://wiki.inkscape.org/wiki/index.php/Release_notes/1.1). 


**The [1.1alpha release is available to download Inkscape 1.1alpha for Linux, Windows and macOS](https://inkscape.org/release/)**.


Report bugs and experience related to the alpha by [filling out a report directly at the Inbox](https://gitlab.com/inkscape/inbox/-/issues/).



