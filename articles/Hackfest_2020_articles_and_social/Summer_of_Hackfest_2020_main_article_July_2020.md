main article for Summer of Hackfest, July 11- August 15, 2020 (pre-event publication)

## Inkscape's summer of Hackfest 2020 is ONline!



![hackfest_2020_p15](https://gitlab.com/inkscape/vectors/general/uploads/3569563aa1fffa35df82790df0397beb/hackfest_2020_p15.png)



Inkscape is preparing a [summer of Hackfest 2020 online](https://wiki.inkscape.org/wiki/index.php?title=Hackfest2020_Online) on Saturdays between July 11 and August 15. That's six Saturdays in a row for a six-hour stretch each time via live video. Everyone is welcome!

Inkscape contributor Martin Owens, who stepped up to organize and host the event, explained, "We organise a hackfest every year and they are very useful for social bonding and figuring out what we should do as a project, as well as starting some of the more ambitious projects. This year there was no way to host a physical event so we needed to run one online." 

Instead of a weeklong in-person event, one day each week is an easier format to manage online. 

"The goal is to strengthen existing relationships, make sure different teams and people have a voice, and to attract new people from new places who can learn how to contribute and (learn) who else is involved," said Owens.

In July, meet members of Inkscape's board of directors and learn more about the project in general (July 11), dig into user experience priorities through a papercuts hunt (July 18) or participate in more technical discussions, such as a peer code review of merge requests (July 25). 

In August, meet and learn about the Vectors Team and how this group works to promote Inkscape (August 1). Discover how the Google Summer of Code mentors and students are working to improve Inkscape (August 8). Finally, if you've ever wondered how to test and report a bug, here's your chance to learn (August 15)!  

* July 11  - Board of Directors - Meet the board with Bryce Harrington - Come talk about Inkscape
* July 18  - UX - User Experience Priorities with Adam Belis - Papercuts Hunt
* July 25  - Developers - Technical discussions with Thomas Holder - Peer code review of existing merge requests
* Aug.  1  - Vectors - Vectors 2.0 with Ryan Gorley 
* Aug.  8  - Mentors - Google summer of Code showcase with Tavmjong Bah
* Aug. 15  - Testers - Bug Management with Nathan Lee - Test a bug and report back 

[Sessions will start at 4 pm UTC](https://www.timeanddate.com/worldclock/converter.html?iso=20200711T160000&p1=281&p2=179&p3=137&p4=136&p5=220) with an initial presentation of the topic and the outline of the activities or discussion to follow. 

Join in Inkscape's [summer of Hackfest 2020](https://gitlab.com/inkscape/vectors/content/-/issues/86) at any point - for a few minutes or a few hours - by clicking here (link to BBB Hackfest room). Watch for more news each week. See you there! 








