Inkscape 1.0 release 

Main article for Contributor Interviews 



**A GLIMPSE INTO THE WORLD OF INKSCAPE CONTRIBUTORS**

**Programmers, artists, students, designers, translators - volunteers from around the world collaborating on the future**



In honor of Inkscape's 1.0 release, we asked contributors how and why they joined the project community, what they worked on for this latest version, and what they love about it the most. 

If you're not familiar with open source projects, it's important to understand that the community has no one leader, so contributors are equal and can choose what they want to work on. Many decisions are reached through collaboration and work is done voluntarily. 

The community uses open source tools to get the job done - meaning that sometimes for newcomers to the project it can seem like "everything is oriented for Linux programmers and takes the long way around." That's because it's true. Above everything else, this community is friendly, collaborative, talented, driven, and up to the challenge of solving whatever's in their way on the road to creating the best possible open source vector editor in the world. 

Come for Inkscape support, stay for the friendships and opportunities to contribute something that can help make a difference in the world.

The collaborative work of building, supporting and promoting Inkscape through volunteer hours, takes more time; there's a different rhythm here, a different perspective that tends to draw us closer and make us respect one another for our ? perspectives.

Contributors from over a dozen countries around the world shared their stories with us. We thank them for offering up a little slice of their experience just for you.

Oh, and if you're curious enough to consider joining us, you'll find some good advice about that, too.

Happy reading. Draw freely!  



_______

We'll be adding more stories here over the next while, so be sure and come back for more or **follow us on social media**(*link to our social media list*) and be among the first to know.



In order of appearance in the interviews: 

1. Anonymo_i_us
2. C. Rogers
3. Jabierxtof
4. Duarte Ramos
5. Beck
6. Ryan Gorley
7. Yichuang ?
8. Andrew
9. doctormo
10. Sonia Bennett
11. Utkarsh Dwivedi
12. Jonathan
13. Sergei
14. Rania Amina
15. Gellért Gyuris
16. Shlomi Fish
17. José Esteban Quintanilla Ramírez
18. Thomas Holder
19. Patrick
20.  Cedric Gémy
21. Adam Belis
22. Marcin
23. René
24. (teenager)
25. Tim Jones



