Inkscape 1.0 release - April 2020 interview

*Formulaire : [Inkscape Contributor Interview](https://framaforms.org/inkscape-contributor-interview-1587833730)*

*Soumis par Anonyme (non vérifié)*

*sam, 25/04/2020 - 19:14*

*0.0.0.0*



**Contributor Interview: 驿窗 Yichuang** 



**Inkscape "represents the future"**



**Please introduce yourself; what is your name and where in the world do you live?**

Open Source Software user & researcher; Yichuang 驿窗; China



When did you first meet the Inkscape Community? Please explain how you came to join the project.

In March 2020 to translate the User Interface.



**What are the areas and/or teams of the project that you contribute to or have contributed to?**

The translation of the UI; I have published inkscape online video tutorials and provide free online Q & A services.



**What motivates you to contribute to the Inkscape project? What do you like about the community?**

It is Open source, it is free; there are no commercial restrictions and I can run it on a linux system.



**Please name and explain a few things that you worked on for Inkscape 1.0. Why did you choose to work on those ones?**

Translator, UI; video tutorial maker; online Q & A provider.



**What's your favorite part of the 1.0 release?**

Split view mode



**If you could wave a magic wand, what one thing would you like to see in the Inkscape project?**

Maybe really pressing support (CMYK) like Scribus, such as color management/spot color/color separate.



**What would be your best advice to anyone wanting to contribute to the Inkscape project?**

It represents the future.



**Is there anything else you'd like to tell us?**

Inkscape is a wonderful project.



_________________

How can we contact you for details / clarifications?classenu@163.com