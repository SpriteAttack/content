# **Roots and Shoots of the Inkscape Project **

## **Helping people develop software & artistic talents through direct participation** 



*As we launch Inkscape 1.0, we're proud to unveil a collection of stories, woven from interviews and thank-yous, from the Inkscape project's many contributors. This is the first article in the series.* 

*While we come from many countries large and small across this vast blue planet, our commitment and dedication to Inkscape bring us together, unite us in friendship and comradery, thanks to the power of open source and the web.* 

The Inkscape project's roots sprouted, in true open-source fashion, from a fork (branch) of an earlier project, Sodipodi, back in late 2003.  (*link to definitions: https://opensource.com/article/18/7/forks-vs-distributions*)

Among those who were part of Inkscape's début is software developer Bryce Harrington, from Oregon, USA, who chairs the project's Board of Directors and can still be spotted at some Hackfests and engaging in a number of the team chats.   

"It's hard to believe that so many years after we started Inkscape, that it's still going strong, powered almost entirely by volunteers. We knew we had the seeds of a great piece of software when we started the project, but had no idea how widespread and popular it would become, said Harrington.

It's hard to put an exact number on just how many users benefit from Inkscape to express their creative, scientific, and other ideas. The fact that the software is freely available enables a range of users from different spheres to create with it. Inkscape's chat spaces, forum, gallery and social media channels let us gain insights into the broad spectrum of individuals, skill sets and reasons of our users.

As with many open source projects, a certain segment of users often find their way to Inkscape in search of an answer, report a bug or figure out how to build an extension to solve their particular problem. 

"A founding principle was that the project be welcoming and open to new contributors and new ideas; this has enabled a close involvement of the userbase in the software's development, which has helped ensure Inkscape is relevant to real-world needs and given a strong partnership with the community to promote and support artists around the world," added Harrington.

Those who are curious - or brave - enough to stick around - users, developers, designers, artists - become valuable contributors, mentors, and valued members of the Inkscape Team. Often, contributors come in with one set of skills and develop other ones within our encouraging community. 

 José Esteban Quintanilla Ramírez, a graphic designer, freelancer and teacher from Nicaragua, downloaded Inkscape and, while exploring the program, found a bug. Remembering information on our web site about reporting issues, he thought, "Can I report this as a bug? Is it a program failure? Am I doing something wrong? I thought people would laugh at me. Who was I to point out a bug in a program? I am not a programmer, but I am a user now." 

He decided to send the message. "To my surprise somebody answered my email and we talk about it and other things. Then Inkscape fixed the problem and that was proof for me that you really hear the people that use your product. I decided to continue using the program and reporting things." Since then Ramírez has joined the translation team and helps with ensuring that users can stay informed in Spanish.

Designer and developer Jabiertxof, from Spain,explained contributing to the project as being "like a drug for me. I can't leave ;)  - I'm not a native English speaker, but our active community is so small, with lots of work, so it makes me happy, to be one of the team." Jabiertxof can be found popping in and out of many team chat channels, keeping things running behind the scenes. He's one of Inkscape's LPE devs, who worked on long list of them for the 1.0 release, including Measure Segments, Offset and the LPE dialog. 

"On a more personal note," said Harrington, "I've learned so much as a software developer through my participation in this project, and credit the last decade of my career as building on skills and processes I gained practice in through Inkscape. The Inkscape project has made a priority to help people develop their software and artistic talents through direct participation in it."



Interested in finding out more about the Inkscape project community? Come say hello [here](https://inkscape.org/community/) and let us know what you're creating with the [latest version of Inkscape](https://inkscape.org/release/inkscape-1.0/?latest=1). Draw Freely!






