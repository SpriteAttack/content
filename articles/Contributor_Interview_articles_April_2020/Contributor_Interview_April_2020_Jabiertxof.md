Inkscape 1.0 release - April 2020 interview

*Informations sur la soumission*

*Formulaire : [Inkscape Contributor Interview](https://framaforms.org/inkscape-contributor-interview-1587833730)*

*Soumis par Anonyme (non vérifié)*

*sam, 25/04/2020 - 17:45*

*0.0.0.0*

**Contributor Interview: Jabiertxof**



Headline 





**Please introduce yourself; what is your name and where in the world do you live?**

Jabiertxof, Spain



**When did you first meet the Inkscape Community? Please explain how you came to join the project.**

My first encounter was with su_v many years ago when I switched to Linux. I wanted to have BSPline-like  ?? (**like Creature House Expression**) and had never touched C++, only PHP. This friendly community helped me to finish the project.

**What are the areas and/or teams of the project that you contribute to or have contributed to?**

I've helped with LPEs, UI, IRC bridge, Tools Improvements...



**What motivates you to contribute to the Inkscape project? What do you like about the community?**

It's like a drug for me. I can't leave ;) I'm not a native English speaker, but our active community is so small with lots of work, so it makes me happy be one of the team.



**Please name and explain a few things that you worked on for Inkscape 1.0. Why did you choose to work on those ones?**

Measure segments LPE, Offset LPE, LPE dialog... there's a long list. I chose this kind of thing because I like it. If I need to, I work in other areas that are important to the project, like documentation, feedback, translation, **spread**, bug triage; they're okay to work on, but not as much fun.



**What's your favorite part of the 1.0 release?**

The migration to GitLab.



**If you could wave a magic wand, what one thing would you like to see in the Inkscape project?**

Speedy Gonzalez never fails!



**What would be your best advice to anyone wanting to contribute to the Inkscape project?**

It's a good way to learn.



_____________________________



**Is there anything else you'd like to tell us?**

Thanks for create the form!

**We'd like to tag you on social media if we use quotes from your interview. If you are on any of the following platforms, please add your handle below:**

Mastodon: https://vis.social/@jabiertxof
Twitter: https://twitter.com/jabiertxof
Facebook: https://www.facebook.com/jabiertxof

How can we contact you for details / clarifications?jabiertxof in RC