Article -- 2019 Inkscape Cover Art Story - Q & A with winner - 



# Q & A with 2019 Inkscape Cover Art Winner Sanda Krstulovic



Congratulations to Sanda Krstulovic, this year's winner of the Inkscape Cover Art Contest. Krstulovic's striking black and white design was voted top design by her fellow members of the online community.

The contest ran from February 5 to March 8, 2019 on the project's **[Facebook public group](https://www.facebook.com/groups/inkscape.org/)**, where participants were encouraged to create and submit their own original work for a chance to be on the cover of the page.



![FB_CoverArt_collage](G:\COMMITMENTS\Inkscape Vectors Team\CoverArt_Story\CoverArt_Story\FB_CoverArt_collage.png)

This year's Inkscape Cover Art Contest generated 24 entries from 22 Inkscapers from around the world. 



Inkscape caught up with Sanda to learn more about how she uses Inkscape. The following interview was conducted via email and has been edited for clarity. 



INKSCAPE: How long have you been using Inkscape and what keeps you using it?

SANDA KRSTULOVIC: I learned the basics of graphic design in a class I took back in 2013. We used Illustrator CS5 in that class, but I was looking for an open source solution I could practice on at home and decided that Inkscape was the best. I got very used to it over time and was impressed how good it was. 

It was about a year and a half ago that I decided I wanted to put my creativity and artistic skills to good use. Inkscape has been my go-to tool for creating illustrations and designs, and I feel like I have leveled up in that time.



INKSCAPE: Do you use Inkscape in your professional life?

SANDA KRSTULOVIC: I use Inkscape to create apparel designs that sell on print-on-demand sites (like Design by humans, Redbubble, Teepublic) under the alias von Kowen. Also, I sometimes do commissions – logos, business cards, posters, flyers, stickers, shirt designs, book or CD covers. I cannot imagine doing all that without Inkscape, so I guess the answer is yes.



INKSCAPE: You are a member of the Inkscape Facebook Group. Has the group helped you in any way, or do you go there to help others?

SANDA KRSTULOVIC: I often read posts in the Inkscape Facebook Group to see what other people can do with it, what kind of problems they face and how to fix them. I like entering the challenges and competitions there. It’s very informative and the group is very well moderated, making it a very positive and supportive environment. I am still too awkward to ask / offer help.



INKSCAPE: Where in the World are you based?

SANDA KRSTULOVIC: I am from a lovely city called Split, in Croatia.



INKSCAPE: What are your interests and hobbies outside of Inkscape?

SANDA KRSTULOVIC: I paint and draw, read books, play bass guitar, watch movies and TV shows. 



INKSCAPE: Your winning design was very popular. What do you think makes your work appeal to others?

SANDA KRSTULOVIC: That design is done in my art style - I enjoy making black and white creepy art. I think it was very noticeable because it stands out or maybe people think tentacles (or octopi’s arms) are cool or because I used the Inkscape logo to make the Inktopus; maybe it is because it’s symmetrical… I am quite surprised I won, to tell you the truth. 



INKSCAPE: What advice would you give to someone trying Inkscape for the first time?

SANDA KRSTULOVIC: Take your time and learn the basics first. If you have chance, enroll in a class to get you started. There are a lot of online courses, tutorials and books that can help you out. You can start by checking out the “Learn” section on inkscape.org. That has been a great help to me. Remember to take it one step at a time; it gets frustrating at times, but don’t give up.



INKSCAPE: What one thing do you use most often in Inkscape and why?

SANDA KRSTULOVIC: I still think of myself as a beginner user and all my designs are basically weird black and white shapes created with the pen tool. I draw basic shapes and then mess around with paths (usually node editing and Boolean operations), do some object manipulation, add some shapes, and somehow it looks OK in the end! 

So… I guess the answer to this one is the pen tool? It was hard for me to figure out Bezier curves when I was learning vector graphic editing. It did not come natural to me - so I spent a lot of time practicing it. I used to draw a design by hand, scan it and trace it with the pen tool for practice, and I ended up loving those curves! 



INKSCAPE: What would you like to see added to Inkscape that would benefit you most?

SANDA KRSTULOVIC: I’ve been thinking about this and can’t think of a good answer. I am so used to Inkscape and am very satisfied with its functionality. If I ever have a problem, I always find a solution thanks to great people who share their experience and are ready to help others. If I have to say something – I sometimes need a .jpeg of my work and can’t export that directly from Inkscape. That function would help me save a few minutes.



INKSCAPE: Last question... Do you think being part of the Inkscape community helps you in any way?

SANDA KRSTULOVIC: People contributing to Inkscape are amazing and I have learned so much from them! So a big thank you to everyone who shares their experiences, tutorials, tips and tricks and helps new users with their problems. You guys are awesome!



Thanks to Sanda for responding to our questions and for her winning design! 

If you use Inkscape for design work and would like to share what you love about the program with us, [get in touch](https://www.facebook.com/groups/inkscape.org/)! 

