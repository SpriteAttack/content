**Inkscape's SCALE17x Hackfest 2019 launched plans for 1.0 release and more**

The California rain did nothing to dampen the spirits of Inkscape developers as they gathered for their first Hackfest of the season, held from March 4-6, 2019, several days before the 17th annual Southern California Linux Expo (SCaLE 17x) conference, in Pasadena.  Space for the hackfest was kindly sponsored to us by the Pasadena City College [link: [https://pasadena.edu/\].](https://pasadena.edu/%5D.)"

"Seven core Inkscape developers met for three solid days to tackle issues and planning important to getting the 1.0 release out the door.  A native Mac OS build is high on everyone's priority list, so the hackfest attendees arranged an online meetup with Inkscape's Mac contributors to plan achieving this important goal.  The merge request queue received ample attention, and several plans established for better onboarding and integration of new contributors to their areas of interest."

"At SCaLE, the attendees made good use of the event for discussions on fundraising and learning from other open-source projects, including as Krita and Homebrew, and the Software Freedom Conservancy.  Work has begun on learning more about opportunities and establishing concrete features to move forward.


*Catch SCaLE17x presentations*

If you're curious to find out what happened at SCaLE17x, videos of the presentations can be viewed [here](https://www.youtube.com/user/socallinuxexpo/videos?sort=dd&view=0&shelf_id=1). 


*Inkscape Hackfest 2019 in Saarbrücken, Germany, May 27-29*

This May, Inkscape developers and contributors will once again be gathering to advance the project for several days prior to attending the LibreGraphics2019 meeting in Saarbrücken, Germany [LibreGraphics2019 meeting](https://libregraphicsmeeting.org/2019/) from May 29 to June 2. 

In addition to working on the project, Inkscapers Jabier Arraiza (@Jabiertxof) and Mihaela Jurkovic (@prkos) will also present at the conference. Watch for more details on their workshop, Explore Inkscape—from the Basics to the Latest Features, and presentation, Get the Most Out of Live Path Effects. 

During this Hackfest, there will be a big push to fix priority bugs, conduct testing with respect to the 1.0 release. A push to recruit new developers to the project will also be discussed.  

Contributors will continue discussions on fundraising and outreach to increase donations to the project. 

Head [here](http://wiki.inkscape.org/wiki/index.php/Hackfest2019_Saarbr%C3%BCcken_Topics) for more details on topics to be tackled during this Hackfest.

Meanwhile, if you're an Inkscape user and you'd like to contribute to the project, here's your chance to make a difference! 

Whether you're an individual, organization or company that uses Inkscape, we would love to know how you use the program to accomplish your goals. Please consider contributing to help advance work on improving this free, open-source solution!

Head [here]((https://inkscape.org/support-us/hackfests/)) to make that contribution today! Thank you.




CAPTION FOR PHOTO FROM SCALE HACKFEST - taken by Ryan Gorley
From left to right: Ted Gould, Krzysztof Kosiński, Tavmjong Bah, Bryce Harrington, Josh Andler, Ryan Gorley & Martin Owens.





**Social Media messages** 

TWITTER & MASTODON

**Post 1**

Happy days! #Inkscape devs & contributors will gather May 27-29 for a 2nd Hackfest in 2019 in Saarbrücken, Germany, prior to the @LGMeeting 2019. They're investing time & energy towards advancing the project. Head here to contribute to such progress: https://inkscape.org/support-us/hackfests/



**Post 2**

Turn your "heart emoticon" for Inkscape into support for our Hackfest. Help defray costs for our continued progress on our RoadMap. Thanks from the bottom of our "heart emoticons"! (https://inkscape.org/support-us/hackfests/)



**Post 3**

DIY #Inkscapers Jabier Arraiza (@Jabiertxof) and Mihaela Jurkovic (@prkos) are presenting at @LGMeeting 2019? You might even find them at the #Inkscape booth. Drop by and say hi! 

*would need a photo of/from the booth here*



**Post 4**

Catch #Inkscape developer Jabier Arraiza (@Jabiertxof) at @LGMeeting 2019 presenting "Get the Most Out of Live Path Effects" on June 2: - add direct link when updated



**Post 5**

Catch #Inkscaper Mihaela Jurkovic (@prkos) at @LGMeeting 2019 presenting "Explore Inkscape—from the Basics to the Latest Features" on May 31: - add direct link when updated



FACEBOOK 

**post 1**

Happy days! #Inkscape developers & contributors are gathering from May 27-29 for Hackfest2019 Saarbrücken prior to the LibreGraphics2019 meeting. 

They're investing time & energy towards advancing the project. Fueling up on coffee.

Head here if you'd like to contribute to such progress: https://inkscape.org/support-us/hackfests/

Thank you from the bottom of our "heart SVG??"!

*(potential image - red cup from gallery online: https://media.inkscape.org/media/resources/file/tasse_rouge_1.png) Or something more interesting!*



**Post 2**

DIY #Inkscapers Jabier Arraiza (@Jabiertxof) and Mihaela Jurkovic (@prkos) are presenting at @LGMeeting 2019? You might even find them at the #Inkscape booth. Drop by and say hi! 

*Would need a photo of/ from the booth here!*



**Post 3**

Catch #Inkscape developer Jabier Arraiza (@Jabiertxof) at @LGMeeting 2019 presenting "Get the Most Out of Live Path Effects" on June 2: - add direct link when updated



**Post 4**

Catch #Inkscaper Mihaela Jurkovic (@prkos) at @LGMeeting 2019 presenting "Explore Inkscape—from the Basics to the Latest Features" on May 31: - add direct link when updated

