**1) Main Congrats social for Top 5 Finalists**

1/2 Ahem... The first round of votes are in!! Thanks to the 96 Inkscape artists for their 124 entries and the 598 community members who took the time to cast a vote!

2/2 Drum roll, please... Congratulations to our Community's Top 5 Finalists in the Inkscape version 1.0 About Screen Contest!! And they are... #5 Rania Amina, #4  Dismecha, #3 Bayu Aji, #2 Bożena Augustyn, & #1 Bayu Rizaldhan Rayes!! 



**2) Whole Congrats social for Top 5 Finalists in one post** 

Ahem... The first round of votes are in!! Thanks to the 96 Inkscape artists for their 124 entries and the 598 community members who took the time to cast a vote! Drum roll, please... Congratulations to our Community's Top 5 Finalists in the Inkscape version 1.0 About Screen Contest!! And they are...  #5 Rania Amina, #4  Dismecha, #3 Bayu Aji, #2 Bożena Augustyn, & #1 Bayu Rizaldhan Rayes!!    





**Web Article for Top 5 Finalists** 

**Community Chooses Top 5 Finalists in version 1.0 About Screen Contest**

The Inkscape Community has spoken! The Top 5 Finalists of the version 1.0 About Screen Contest have been chosen by 598 community voters. 



These five finalists will now go on to the next round of voting by the project's developers who will choose the final design. The winner will be announced on Tuesday, December 17. 

Version 1.0 of Inkscape is already shaping up to be something special, not only for the improvements lovingly crafted in the program itself, but also for the love from the 96 Inkscape artists who designed 124 entries for our About Screen Contest. 

Congratulations to our Top 5 Finalists! 

1. [Bayu Rizaldhan Rayes](https://inkscape.org/~bayubayu/%E2%98%85island-of-creativity)

2. [Bożena Augustyn](https://inkscape.org/~bozena/%E2%98%85inkscape-10-graphic-design)

3. [Bayu Aji](https://inkscape.org/~bajinra/%E2%98%85migrate-to-freedom)
4. [Dismecha](https://inkscape.org/~Dismecha/%E2%98%85how-to-fold-a-memory)
5. [Rania Amina](https://inkscape.org/~raniaamina/%E2%98%85inkscape-10-about-screen-by-rania-amina)



Kudos to everyone who submitted an entry. We are so honored that you took the time to share your passion and your art with us. 

As mentioned in the contest rules, all entries will also be considered for different Inkscape promotional visuals, including social media cover art, [web site cover art](https://inkscape.org/), and Inkscape hackfest or other project event posters, banners and stickers.  

The next round of voting has begun by the project's developers who will choose the final winner whose design will be part of the version 1.0 build and launch. 

We'll announce the winner here a little later this month. In the meantime, if you're curious to see how the About Screen has evolved over the years, head here and scroll down [to check out our gallery](https://inkscape.org/community/about-screen-contests/).

Interested in contributing to the Inkscape project? Visit us online to find out how you can make a difference by [joining our community](https://inkscape.org/community/) or donating to [support our work](https://inkscape.org/support-us/)!









