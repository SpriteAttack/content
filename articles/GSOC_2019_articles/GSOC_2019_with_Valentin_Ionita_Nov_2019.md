(GSOC 2019 with @vanntile) 


Valentin wrangled meshes, hatches and GTK during a summer with Inkscape in 2019 

When the Inkscape community seeks Google Summer of Code (GSoC) applicants, we're hoping to find people who are curious about the project and the software. Part of the application process requires applicants to complete sample work on the project to demonstrate their skills and understanding.

In the winter of 2019, Valentin (@vanntile) joined the community, tackled several tasks and shared the outline of a project he wanted to work on as a GSoC student. 

"The GSoC experience was something I had been waiting for a whole year, since the first time I had heard of a program like this. When I found Inkscape to be one of the organizations, a program I had been using for such a long time, I decided to be one of the mentees," said Valentin, who hails from Bucharest, Romania.

By the summer, our GSoC student not only knew the program, but got involved in our online community. Valentin brought his curiosity, determination and coding skills to the project. Inkscape core developer Tavmjong Bah (@Tav) mentored Valentin during his work.

Valentin proposed working on JavaScript polyfills to improve image rendering in browsers for mesh gradients and SVG hatch fills. While it began there, his contribution evolved to working on mesh gradients and hatches to enable non-technical users of Inkscape to more easily use them. 

His work on mesh gradient polyfills aimed to improve the code quality by switching to "modern JavaScript syntax without losing performance [and] increasing performance, if possible". The next part of this work had Valentin digging back into theories in mathematical textbooks (bilinear interpolation is both great and difficult, according to him) and Wikipedia to solve the problem. (For a dictionary of terms used in this article, head to the end of the page.)
 

![pepper_rendered][1]
*Evolution of the mesh rendering script output*


"Hatches," he explained, "are specialised paint servers that repeat one or several `hatchpaths` by a fixed formula based on the element's attributes - x, y, rotate and pitch." 

Valentin worked on improving the UI and UX of hatches in the Inkscape interface. This meant adding "editing handles for hatches that appear when you have an element with a hatch and use a specific set of tools, for example, the node-editing tool. Using the handles, you can rotate, scale and translate the paint server."

While he experienced a steep learning curve with managing the components of the user interface, in the end, "the best source of understanding complex behavior using GTK widgets was actually Inkscape." 

Working on "improving the actual Inkscape interface and functionality concerning paint servers", Valentin explained that "This was completely different for me, passing from a scripting language, JavaScript, to a programming one, C++, and, especially, the GTK framework to manage the interface". 

Then Valentin worked on improving the support of hatches in Inkscape. "My work tried to add small UI elements available to other paint servers (i.e. patterns) to the hatches and introduce a new dialog in Inkscape with the purpose of previewing and setting the paint server." This work will serve in the development of a new tool in the program to manage, preview and set all types of paints from resource files (i.e. an SVG file that contains a list of patterns, hatches, swatches, etc.) within Inkscape and from users. 

Inkscape definitely gained much through Valentin's contribution, which continues to this day. Understanding how contributing to Inkscape helped him is also part of our learning as a community. 

Among the takeaways for Valentin, include code-specific experiences and learning, he learned that "patience is required when exploring solving a problem; on a decentralized project, involvement in secondary features is the duty of the independent developers, not the main team, and asking the community can be the greatest source of truth."

Finally, he said, "I appreciate everyone from the Inkscape team, whether they are in the dev, ux or the vectors teams. Thank you!"

We thank Valentin for his ongoing contribution to this project. We're excited to see what he contributes next! 

Check out Valentin's full-length documentation ( https://gitlab.com/vanntile/inkscape-gsoc-application ) on the work he accomplished during his GSoC in 2019.

If you're interested in becoming an Inkscape contributor, head here ( https://inkscape.org/contribute/ ) and then consider joining our community ( https://inkscape.org/community/ ). If you're interested in applying for GSOC 2020, head here (LINK TO PROMO ARTICLE).

## Dictionary of terms
- `widget` = dialog element
- `pitch` = the distance between the repetitions of the hatch units along the axis defined by the *rotation*.
- `polyfill` = a piece of javascript code that adds functionality to a web browser. The functions it adds were parts of the standard at one point, but the browsers are a bit behind, leaving both mesh gradients and hatches outside the standard. Basically, he worked on making SVGs with meshes and hatches display properly in a browser, even if the browser has no clue what a hatch or mesh is.
- `paint server` = According to the W3C [specification](https://www.w3.org/TR/SVG/pservers.html), a paint server is "a method which allows the fill or stroke of an object to be defined by a resource found elsewhere. It allows resources to be reused throughout a document." Some notable paint servers (adopted or not at the moment of speaking) include flat colors, gradients, mesh gradients, patterns, hatches. Inkscape uses all of the above, even though through different dialogs and UI.

[1]: https://gitlab.com/vanntile/inkscape-gsoc-application/-/raw/master/graphics/pepper_rendered.jpg