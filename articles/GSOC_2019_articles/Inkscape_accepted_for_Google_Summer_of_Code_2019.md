**Seeking Google Summer of Code students to work for Inkscape this summer**

Inkscape has been approved to accept students to work on the project through Google Summer of Code (GSOC) 2019. This means Inkscape is, once again, going to be able to offer students a chance to work with mentors on this open source project.

If you're interested in applying to work with us on the project this summer, get in touch!

In order to apply, you will need to contribute to the Inkscape project.

Here are the steps you need to take before applying: 

1. Read the Wiki page: http://wiki.inkscape.org/wiki/index.php/Google_Summer_of_Code

2. Read the GSOC student guide: https://google.github.io/gsocguides/student/

3. Introduce yourself on #inkscape-devel on IRC or on https://chat.inkscape.org/channel/team_devel

4. Identify and contribute two patches (small code contributions) to the project  


As you are working on your patches, think about the code project you wish to propose to improve Inkscape.

Once you have introduced yourself, discuss your plans with the development team as you will be collaborating on the GSOC application. The wiki link above outlines the Inkscape project, potential GSOC project areas and mentors.  

Inkscapers will be happy to answer your questions about how to get involved.

The Student Application Period runs from March 25 to April 9, 2019. For more details, visit the GSOC home page (https://summerofcode.withgoogle.com/). Then head to the Inkscape page on GSOC (https://summerofcode.withgoogle.com/organizations/5311282658410496/) for the specific questions and details 

The deadline for applying to the Google Summer of Code is April 9, and it is coming up quickly! 

We look forward to meeting you soon. 



 
