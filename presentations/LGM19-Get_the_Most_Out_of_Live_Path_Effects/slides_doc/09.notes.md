# SLIDE 9
![LPE can be added in stack](previews/09.png "LPE can be added in stack")
## SLIDE CONTENT
### LPEs can be stacked
You're not limited to using one LPE only for one object. You can also repeat an LPE.  
For example, here with the **Mirror Symmetry** and the new ***Measure Segments LPE**.
## SLIDE NOTES
You're not limited to using one LPE only for one object. You can also repeat an LPE.  
For example, here with the **Mirror Symmetry** and the new ***Measure Segments LPE**.
## SLIDE NOTES PRONOUNCE
