# SLIDE 3
![ What kind of items work with LPE?](previews/03.png "What kind of items work with LPE?")
## SLIDE CONTENT
### What kind of items work with LPE?
It depends on the LPE selected. You can use them on groups, shapes and paths.  
Clones have a special workaround. We're looking at future support for text and images.

## SLIDE NOTES
Each LPE can be applied to one or more types of objects. The original goal was to 
work only on paths, but over time, we added support for shapes, clones and groups.  
We want to add support for more elements, like text and images, in the future.

## SLIDE NOTES PRONOUNCE
...

