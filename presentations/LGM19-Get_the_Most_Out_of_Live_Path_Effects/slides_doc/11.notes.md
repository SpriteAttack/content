# SLIDE 11
![ Offset LPE, offsetting like never](previews/11.png " Offset LPE, offsetting like never")
## SLIDE CONTENT
### The power of offset in LPE
This improved LPE now allows you to expand a path retaining cusp nodes or to add an extrapolated arc to it.
## SLIDE NOTES
This improved LPE now allows you to expand a path retaining cusp nodes or to add an extrapolated arc to it.
Extrapolated arcs and offsetting are based on the work of Liam. P. White and Kristof Kozinski.
## SLIDE NOTES PRONOUNCE
...

