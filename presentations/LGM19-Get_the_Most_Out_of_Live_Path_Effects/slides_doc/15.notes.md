# SLIDE 15
![LPE Defaults](previews/15.png "Defaulting parameters in LPEs")
## SLIDE CONTENT
### LPE parameter default settings
You can set default parameters for each LPE.
## SLIDE NOTES
Each LPE parameter can be set with defaults for future use.
## SLIDE NOTES PRONOUNCE
...

